export default function FormDataHandler(e: any) {
  const formData = [...e.currentTarget.elements].reduce(
    (current, prev) => ({ ...current, [prev.name]: prev.value }),
    {}
  );
  delete formData[""];

  return formData;
}
