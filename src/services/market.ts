
import axios from "axios";
export const GetMarketPriceApi = (coins?: string[]) => {
  let apiCall = axios.get(
    `${process.env.NEXT_PUBLIC_BINANCE_URL}/ticker/price?${
      coins ? "symbols=" + JSON.stringify(coins) : ""
    }`
  );
  return apiCall;
};

export const GetSymbolLogApi = (coins?: string) => {
  let apiCall = axios.get(
    `${process.env.NEXT_PUBLIC_BINANCE_URL}/trades?symbol=${coins}&limit=40`
  );
  return apiCall;
};
