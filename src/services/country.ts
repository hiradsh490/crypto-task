import useFetch from "@/hooks/useFetch";
export const GetCountryApi = () => {
  let apiCall = useFetch(null).get("/api/country");
  return apiCall;
};

export const GetCountriesCityApi = (_country: string) => {
  let apiCall = useFetch(null).get(`/api/country/${_country}`);
  return apiCall;
};
