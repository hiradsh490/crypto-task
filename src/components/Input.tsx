import React from "react";

interface Props {
  label?: string;
  required?: boolean;
  type: string;
  onInput: (name: string, value: string) => void;
  name: string;
  isDisable?: boolean;
  value?: any;
}

const Input = (props: Props) => {
  const { label, type, value, name, onInput, required, isDisable } = props;
  return (
    <div>
      {label && (
        <label
          htmlFor={label}
          className="block text-sm font-medium leading-6 text-gray-900"
        >
          {label}
        </label>
      )}

      <div className="mt-2">
        <input
          id={label}
          disabled={isDisable}
          type={type}
          name={name}
          autoComplete={name}
          required={required}
          onInput={(e: any) => onInput(name, e.target.value)}
          className="block w-full px-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6"
        />
      </div>
    </div>
  );
};

export default Input;
