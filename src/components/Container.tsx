import React from 'react'

interface Props{
    children:any
}
const Container = (props:Props) => {
    
  return (
    <section className='container mx-auto'>{props.children}</section>
  )
}

export default Container