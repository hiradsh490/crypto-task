import Input from "@/components/Input";
import SelectBox from "@/components/SelectBox";
import React, { useMemo, useState } from "react";
interface Props {
  prices: any[];
}
const options = ["EURO", "USDT"];
const ConvertBox = (props: Props) => {
  const [convertStatus, setConvertStatus] = useState({
    from: "",
    to: "",
    amount: 0,
  });
  const [result, setResult] = useState<number>();
  const { prices } = props;

  const convertor = useMemo(() => {
    if (!prices) return;
    const { from, amount, to } = convertStatus;

    if (from === to || from === "USDT")
      return setResult(amount * prices[0]?.price);

    setResult(amount / prices[0]?.price);
  }, [convertStatus]);

  return (
    <div className="p-5 rounded-md mt-10  border border-solid border-gray-200 gap-5 shadow-sm ">
      <SelectBox
        onChange={(value: string) =>
          setConvertStatus({ ...convertStatus, from: value })
        }
        options={options}
        label="convert from"
        name=""
      />
      <SelectBox
        onChange={(value: string) =>
          setConvertStatus({ ...convertStatus, to: value })
        }
        options={options}
        label="convert to"
        name=""
      />
      <Input
        onInput={(_, value: string) =>
          setConvertStatus({ ...convertStatus, amount: value })
        }
        type="number"
        name=""
      />
      <span className="block w-full px-2 h-9  mt-2 rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6">
        {result}
      </span>
    </div>
  );
};

export default ConvertBox;
