import React from "react";

const DashboardHeader = () => {
  return (
    <header style={{padding:"20px 0"}} className=" shadow-lg p-10">
      <h1 className="font-bold text-2xl text-center">Crypto Dashboard</h1>
    </header>
  );
};

export default DashboardHeader;
