import { GetSymbolLogApi } from "@/services/market";
import React, { useEffect, useState } from "react";
import Chart from "react-apexcharts";

const CandleChart = () => {
  const [series, setSeries] = useState<any>();
  const [chartRowLabel, setChartRowLabel] = useState([]);
  const options = {
    chart: {
      height: 500,
      zoom: {
        autoScaleYaxis: true,
      },
    },
    dataLabels: {
      enabled: false,
    },
    markers: {
      size: 0,
      style: "hollow",
    },
    xaxis: {
      categories: chartRowLabel,
      tickAmount: 6,
      labels: {
        style: {
          // colors: colors,
          fontSize: "12px",
        },
      },
    },
    fill: {
      type: "gradient",
      gradient: {
        shadeIntensity: 1,
        opacityFrom: 0.7,
        opacityTo: 0.9,
        stops: [0, 100],
      },
    },
  };

  useEffect(() => {
    getLog();
  }, []);

  const getLog = async () => {
    try {
      let response = await GetSymbolLogApi("EURUSDT");
      setSeries([
        {
          data: response.data.map((item: any) => item.price),
        },
      ]);
      setChartRowLabel(response.data.map((item: any) => convertMillieToDate(item.time)));
      setTimeout(() => {
        getLog();
      }, 2000);
    } catch (error) {
      console.log(error);
    }
  };

  function convertMillieToDate(millie: number) {
    const time = new Date(millie);
    return time.toLocaleTimeString();
  }

  return (
    series && (
      <div className="flex justify-center mt-10">
        <Chart options={options} series={series} type="area" width="500" />
      </div>
    )
  );
};

export default CandleChart;
