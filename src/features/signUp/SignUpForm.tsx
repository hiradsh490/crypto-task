"use client";
import React from "react";
import Input from "@/components/Input";
import SelectBox from "@/components/SelectBox";
import { GetCountryApi, GetCountriesCityApi } from "@/services/country";
import FormDataHandler from "@/utils/formDatahandler";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";

const SignUpForm = () => {
  const [countries, setCountries] = useState([]);
  const [cities, setCities] = useState([]);
  const router = useRouter();

  useEffect(() => {
    if (localStorage?.user) router.push("/dashboard");

    getCountries();
  }, []);

  const getCountries = async () => {
    try {
      let response = await GetCountryApi();
      setCountries(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  const getCity = async (country: string) => {
    try {
      let response = await GetCountriesCityApi(country);
      setCities(response.data.data);
    } catch (error) {
      console.log(error);
    }
  };

  const submitForm = (e: any) => {
    e.preventDefault();

    if (
      !FormDataHandler(e).password.match(
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/
      )
    )
      return toast.error(
        "password should includes least eight chars, special char, and capital char."
      );
    localStorage.setItem("user", JSON.stringify(FormDataHandler(e)));
    router.push("/dashboard");
  };

  return (
    <div className="flex min-h-full flex-1 flex-col justify-center px-6 py-12 lg:px-8">
      <div className="sm:mx-auto sm:w-full sm:max-w-sm">
        <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
          Sign in to your account
        </h2>
      </div>

      <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
        <form onSubmit={submitForm} className="space-y-6">
          <Input
            onInput={() => {}}
            name={"username"}
            label={"username"}
            type={"text"}
            required
          />
          <Input
            onInput={() => {}}
            name={"email"}
            label={"email"}
            type={"email"}
            required
          />
          <Input
            onInput={() => {}}
            name={"password"}
            label={"password"}
            type={"password"}
            required
          />
          <SelectBox
            onChange={getCity}
            options={countries}
            name={"country"}
            label={"country"}
          />
          <SelectBox
            name={"city"}
            options={cities}
            onChange={(value) => console.log(value)}
            label={"city"}
          />

          <div>
            <button
              type="submit"
              className="flex w-full justify-center rounded-md bg-indigo-600 px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
            >
              Sign in
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default SignUpForm;
