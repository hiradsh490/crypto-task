"use client";
import CandleChart from "@/features/dashboard/Chart";
import ConvertBox from "@/features/dashboard/ConvertBox";
import { GetMarketPriceApi } from "@/services/market";
import React, { useEffect, useMemo, useState } from "react";
import { toast } from "react-toastify";

const MainDashboard = () => {
  const [price, setPrice] = useState<any>();
  useEffect(() => {
    getPrice();
  }, []);

  const updatePrice = useMemo(() => {
    price && toast.success("price updated");
  }, [price]);

  const getPrice = async () => {
    try {
      const response = await GetMarketPriceApi([ "EURUSDT"]);
      setPrice(response.data);
      setTimeout(() => {
        getPrice();
      }, 2000);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <ConvertBox prices={price} />
      <ul className="pt-10 grid gap-3">
        {!price
          ? "loading ..."
          : price?.map(
              (item: any, index: number) =>
                index < 20 && (
                  <li
                    className="flex py-2 px-5 rounded-md justify-between items-center border border-solid border-gray-500"
                    key={index}
                  >
                    <span>{item?.symbol}</span>
                    <span>{item?.price}</span>
                  </li>
                )
            )}
      </ul>
      <CandleChart />
    </>
  );
};

export default MainDashboard;
