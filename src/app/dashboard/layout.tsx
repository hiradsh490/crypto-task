import Container from "@/components/Container";
import DashboardHeader from "@/features/dashboard/DashboardHeader";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <section>
      <DashboardHeader />
      <Container>{children}</Container>
    </section>
  );
}
