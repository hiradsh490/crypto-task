import SignUpForm from "@/features/signUp/SignUpForm";

export default async function Home() {
  return (
    <>
      <SignUpForm />
    </>
  );
}
