import { NextResponse } from "next/server";
import countries from "@/utils/countries.json";

export async function GET(request: Request) {
  const countryName: string = request.url.slice(
    request.url.lastIndexOf("/") + 1
  );
  return NextResponse.json({ data: countries[countryName] });
}
