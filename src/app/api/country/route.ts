import { NextResponse } from "next/server";
import countries from "@/utils/countries.json";

export async function GET() {
  return NextResponse.json(Object.keys(countries));
}
